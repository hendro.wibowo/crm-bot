import express from 'express';
import axios from 'axios';

import { fbAccessToken, fbPageId, fbWebhookChallenge, fbWebhookActive, fbReplyToAll, sbApiToken } from '../config';

const router = express.Router();

router.route('/')
  // This endpoint will be called by Facebook only once when verifying our webhook
  // Only for this endpoint, you should write the endpoint logic exactly like below if you will write your bot in other programming language.
  .get((req, res) => {
    const params = req.query;

    if (params.hasOwnProperty('hub.mode') && params.hasOwnProperty('hub.verify_token') && params.hasOwnProperty('hub.challenge')) {
        if (params['hub.mode'] === 'subscribe' && params['hub.verify_token'] === fbWebhookChallenge) {
            return res.send(req.query['hub.challenge']);
        }
    }

    return res.status(400).send("Error occurred. Maybe your token was wrong.");
  })

  // This endpoint will receive every webhook event data sent by Facebook and then we send the response to Messenger.
  .post(async (req, res) => {
    try{
      const reqBody = req.body;
      const entryMsg = reqBody.entry || [];


      // START BOT LOGIC
      // Do your logic here
      const messageData = entryMsg[0].messaging[0];
      const fbSenderId = messageData.sender.id;

      console.log(messageData);

      let messageContent = null;

      if (messageData.hasOwnProperty('postback')) {
        messageContent = {
          type: "postback",
          senderId: fbSenderId,
          message: [ messageData.postback ],
        };
      } else {
        const message = messageData.message;

        if (message.hasOwnProperty('attachments')) {
          messageContent = {
            type: "file",
            senderId: fbSenderId,
            message: message.attachments,
          };
        } else {
          messageContent = {
            type: "text",
            senderId: fbSenderId,
            message: [
              {
                text: message.text
              },
            ],
          };
        }
      }

      axios.post('http://159.65.2.43:8888/get.cfm', messageContent, {
        "headers": {
          "Content-Type": "application/json",
        },
      })
      .then(response => {
        console.log(response);

        return res.json({ status: 'OK' });
      })
      .catch(error => {
        console.log(error);

        return res.status(400).json({
          status: 'FAILED',
          message: error.message,
        });
      });
      // END BOT LOGIC
    } catch(error) {
      console.log(error);
<<<<<<< HEAD

      return res.status(400).json({
=======
      return res.json({
>>>>>>> 2c6058a8090fb63575b44a2fcaa3488f3adbb974
        status: 'FAILED',
        message: error.message,
      });
    }
  });

router.route('/send')
  .post(async (req, res) => {
<<<<<<< HEAD
    try{
      const messageData = req.body;
      const messageSenderId = messageData.senderId;
      const messageType = messageData.type;
      const messageText = messageData.text || 'Text message';
      const messageButtons = messageData.buttons || [];
      const buttonPayloads = [];

      let messageContent = null;

      switch(messageType) {
        case 'button':
          for (let i = messageButtons.length - 1; i >= 0; i--) {
            buttonPayloads.push({
              "type": "postback",
              "title": messageButtons[i].title,
              "payload": messageButtons[i].payload,
            });
          }

          messageContent = {
            "recipient": {
              "id": messageSenderId,
            },
            "message": {
              "attachment": {
                "type": "template",
                "payload": {
                  "template_type": "button",
                  "text": messageText,
                  "buttons": buttonPayloads,
                },
              },
            },
          };
          break;

        case 'urlButton':
          for (var i = messageButtons.length - 1; i >= 0; i--) {
            buttonPayloads.push({
              "type":"web_url",
              "url": messageButtons[i].url,
              "title": messageButtons[i].title,
              "webview_height_ratio": "full",
            });
          }

          messageContent = {
            "recipient": {
              "id": messageSenderId,
            },
            "message": {
              "attachment": {
                "type": "template",
                "payload": {
                  "template_type": "button",
                  "text": messageText,
                  "buttons": buttonPayloads,
                },
              },
            },
          };
          break;

        default:
          messageContent = {
            "recipient": {
              "id": messageSenderId,
            },
            "message": {
              "text": messageText,
            },
          };
      }

      axios.post('https://graph.facebook.com/v2.6/me/messages?access_token=' + fbAccessToken, messageContent, {
        "headers": {
          "Content-Type": "application/json",
        },
      }).then((response) => {
        return res.json({ status: 'OK' })
      }).catch(function (error) {
	console.log(error);

        return res.status(400).json({
          status: 'FAILED',
          message: error.message,
        });
      });
    } catch (error) {
      return res.json({ status: 'OK '});
    }
=======
    const message = req.body;

    console.log("Message: " + JSON.stringify(message));

    // Post reply back to user via Messenger Platform API
    // const messageContent = {
    //   "recipient": {
    //     "id": message.recipient.id,
    //   },
    //   "message": {
    //     "text": "Current timestamp: " + Math.floor(Date.now() / 1000),
    //   },
    // };

    // if (message.type === "text") {
    axios.post('https://graph.facebook.com/v2.6/me/messages?access_token=' + fbAccessToken, message, {
      "headers": {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      return res.json({ status: 'OK' })
    }).catch(function (error) {
      return res.status(400).json({
        status: 'FAILED',
        message: error.message,
      });
    });
    // }
>>>>>>> 2c6058a8090fb63575b44a2fcaa3488f3adbb974
  });

export default router;
