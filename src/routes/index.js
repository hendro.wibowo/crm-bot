import express from 'express';

import facebookRouter from './facebook';

export default () => {
    const router = express.Router();

    router.use('/facebook', facebookRouter);

    return router;
}
