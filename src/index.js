import 'babel-core/register';
import 'babel-polyfill';

import express from 'express';
import bodyParser from 'body-parser';
import endpoint from './routes';

const app = express();
const port = process.env.PORT || 1337;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Load routers
app.use('/webhooks', endpoint());
app.use('/', (req, res) => res.send('CRM bot webhooks'))

// Start server
app.listen(port, () => console.log('Server listening at ' + port));
